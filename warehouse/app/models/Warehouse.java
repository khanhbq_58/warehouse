package models;
import javax.persistence.*;
import play.db.ebean.Model;
import java.util.*;
@Entity
public class Warehouse extends Model{
    @Id
    public Long id;
    public String name;
    @OneToOne
    public Address address;
    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();  // trường quan hệ
    public String toString() {
        return name;
    }
}